<?php
/**
 * Chaine de langue pour le plugin Socicon
 *
 * @plugin     Socicon
 * @copyright  2017
 * @author     Teddy Payet
 * @licence    GNU/GPL
 * @package    SPIP/Socicon/Langue
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_explication' => 'Coupler au plugin "Liens vers les réseaux sociaux", ce plugin permet d’utiliser toutes les icones mises à disposition par la librairie Socicon.',

	// L
	'label_oui' => 'Oui',
	'label_non' => 'Non',
	'label_search_icon' => 'Recherche rapide',
	'label_show_checked' => 'Afficher que les icônes sélectionnées',
	'label_socicon_selection' => 'Choisir les réseaux sociaux à utiliser',

	// T
	'titre_page_configurer_socicon' => 'Socicon',
);

