<?php
/**
 * Chaine de langue pour Socicon
 *
 * @plugin     Socicon
 * @copyright  2017
 * @author     Teddy Payet
 * @licence    GNU/GPL
 * @package    SPIP/Socicon/Langue
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'socicon_description' => 'Coupler au plugin "Liens vers les réseaux sociaux", ce plugin permet d’utiliser toutes les icones mises à disposition par la librairie Socicon.',
	'socicon_nom' => 'Socicon',
	'socicon_slogan' => 'La police d’îcones des réseaux sociaux.',
);

