<?php
/**
 * Fichier de fonctions pour le plugin Socicon
 *
 * @plugin     Socicon
 * @copyright  2017-2023
 * @author     Teddy Payet
 * @licence    GNU/GPL
 * @package    SPIP/Socicon/Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Lister le nom des réseaux sociaux mis à disposition par la librairie Sosicon.
 *
 * @return array
 */
function lister_socicon() {
	$style_socicon = find_in_path('lib/socicon/style.css');
	$style_socicon = file_get_contents($style_socicon);

	preg_match_all("/\.socicon-(.+):before/", $style_socicon, $socicon);
	if (isset($socicon[1]) and is_array($socicon[1]) and count($socicon[1])) {
    natsort($socicon[1]);
		return $socicon[1];
	}

	return array();
}

/**
 * Lister les informations des icones à partir du JSON de Socicon > v3.7.2
 *
 * @return array
 */
function lister_socicon_json() {

	$socicon_json_url = find_in_path('lib/socicon/chart-list.json');
	$socicon_json = file_get_contents($socicon_json_url);
	$socicon_json = json_decode($socicon_json);

	if (is_array($socicon_json) and count($socicon_json) > 0) {
		return $socicon_json;
	}

	return array();
}