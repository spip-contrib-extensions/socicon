<?php
/**
 * Définit les autorisations du plugin Socicon
 *
 * @plugin     Socicon
 * @copyright  2014-2023
 * @author     Teddy Payet
 * @licence    GNU/GPL
 * @package    SPIP\Socicon\Autorisations
 */

if (!defined('_ECRIRE_INC_VERSION')) {
  return;
}


/**
 * Fonction d'appel pour le pipeline
 *
 * @pipeline autoriser
 */
function socicon_autoriser() {
}

