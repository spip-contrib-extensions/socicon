# Plugin Socicon

Ce plugin est couplé au plugin "Liens vers les réseaux sociaux" et permet d’utiliser toutes les icones mises à disposition par la librairie Socicon.

La librairie est maintenue sur [Github](https://github.com/Ybbet/socicon) où vous pourrez faire toutes les demandes nécessaires d'ajout d'icônes ou d'évolutions de la librairie. Un site internet a été mis en place pour [présenter la librairie](https://socicon.teddypayet.com) (texte en anglais exclusivement).